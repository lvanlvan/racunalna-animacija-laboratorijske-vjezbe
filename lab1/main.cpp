#include "include/glad/glad.h"
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "include/stb_image.h"

#include "include/learnopengl/shader_m.h"
#include "include/learnopengl/camera.h"
#include "include/learnopengl/model.h"

#include <iostream>

void framebuffer_size_callback(GLFWwindow *window, int width, int height);
void mouse_callback(GLFWwindow *window, double xpos, double ypos);
void scroll_callback(GLFWwindow *window, double xoffset, double yoffset);
void processInput(GLFWwindow *window);

// settings
const unsigned int SCR_WIDTH = 1400;
const unsigned int SCR_HEIGHT = 1000;

// camera
Camera camera(glm::vec3(30.0f, 30.0f, 70.0f));
float lastX = SCR_WIDTH / 2.0f;
float lastY = SCR_HEIGHT / 2.0f;
bool firstMouse = true;
int j = 0, i = 0;

// timing
float deltaTime = 0.0f;
float lastFrame = 0.0f;

int main()
{

    // glfw: initialize and configure
    // ------------------------------
    glfwInit();

    // glfw window creation
    // --------------------
    GLFWwindow *window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "Lab1", NULL, NULL);
    if (window == NULL)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    glfwSetCursorPosCallback(window, mouse_callback);
    glfwSetScrollCallback(window, scroll_callback);

    // tell GLFW to capture our mouse
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    // glad: load all OpenGL function pointers
    // ---------------------------------------
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }

    // tell stb_image.h to flip loaded texture's on the y-axis (before loading model).
    stbi_set_flip_vertically_on_load(true);

    //  load models
    //  -----------
    Model ourModel("objects/Snowflake.obj");
    cout << "ourModel.meshes.size(): " << ourModel.meshes.size() << endl;  
    Model spline("objects/spline.obj");

    // configure global opengl state
    // -----------------------------
    glEnable(GL_DEPTH_TEST);
    std::vector<glm::vec3>
        splinePoints;
    std::vector<glm::vec3>
        splineRotations;
    std::vector<glm::vec3>
        splineCombined;

    splineCombined.push_back(glm::vec3(0.0f, 0.0f, 0.0f));
    splineCombined.push_back(glm::vec3(0.0f, 5.0f, 0.0f));
    splineCombined.push_back(glm::vec3(0.0f, 0.0f, 0.0f));
    splineCombined.push_back(glm::vec3(0.0f, 0.0f, 5.0f));
    splineCombined.push_back(glm::vec3(0.0f, 0.0f, 0.0f));
    splineCombined.push_back(glm::vec3(5.0f, 0.0f, 0.0f));
    splineCombined.push_back(glm::vec3(0.0f, 0.0f, 0.0f));

    glm::mat4x4 B = glm::mat4x4(
        -1.0f / 6, 3.0f / 6, -3.0f / 6, 1.0f / 6,
        3.0f / 6, -6.0f / 6, 3.0f / 6, 0.0f / 6,
        -3.0f / 6, 0.0f / 6, 3.0f / 6, 0.0f / 6,
        1.0f / 6, 4.0f / 6, 1.0f / 6, 0.0f / 6);

    glm::mat3x4 B2 = 0.5f * glm::mat3x4(
                                -1.0f, 3.0f, -3.0f, 1.0f,
                                2.0f, -4.0f, 2.0f, 0.0f,
                                -1.0f, 0.0f, 1.0f, 0.0f);

    for (int i = 0; i < spline.meshes[0].vertices.size() - 3; ++i)
    {
        glm::vec3 position = spline.meshes[0].vertices[i].Position;
        std::cout << "Vertex: (" << position.x << ", " << position.y << ", " << position.z << ")" << std::endl;
        glm::vec3 r0 = spline.meshes[0].vertices[i].Position;
        glm::vec3 r1 = spline.meshes[0].vertices[i + 1].Position;
        glm::vec3 r2 = spline.meshes[0].vertices[i + 2].Position;
        glm::vec3 r3 = spline.meshes[0].vertices[i + 3].Position;
        glm::vec4 rx = glm::vec4(r0.x, r1.x, r2.x, r3.x);
        glm::vec4 ry = glm::vec4(r0.y, r1.y, r2.y, r3.y);
        glm::vec4 rz = glm::vec4(r0.z, r1.z, r2.z, r3.z);
        int iterationCount = 10;
        for (float t = 0.0; t < 1.0; t += 0.02)
        {
            glm::vec4 tvec = glm::vec4(pow(t, 3), pow(t, 2), t, 1);
            glm::vec3 pt = glm::vec3(glm::dot(B * tvec, rx), glm::dot(B * tvec, ry), glm::dot(B * tvec, rz));
            glm::vec3 tvec2 = glm::vec3(pow(t, 2), t, 1);
            glm::vec3 pr = glm::vec3(glm::dot(B2 * tvec2, rx), glm::dot(B2 * tvec2, ry), glm::dot(B2 * tvec2, rz));

            splinePoints.push_back(pt);
            splineRotations.push_back(pr);
            splineCombined.push_back((pt));

            if (++iterationCount >= 10)
            {
                splineCombined.push_back((pt + 0.2f * pr));
                splineCombined.push_back((pt));
                iterationCount = 0;
            }
        }
        // std::cout << "p.size(): " << splinePoints.size() << std::endl;
    }

    unsigned int VBO, VAO, EBO;
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &EBO);
    glBindVertexArray(VAO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, splineCombined.size() * sizeof(glm::vec3), splineCombined.data(), GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, splineCombined.size() * sizeof(glm::vec3), splineCombined.data(), GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void *)0);
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    // draw in wireframe
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    Shader shaderProgram("shaders/spline.vs", "shaders/spline.fs");
    Shader ourShader("shaders/model.vs", "shaders/model.fs");
    

    while (!glfwWindowShouldClose(window))

    {

        // per-frame time logic
        // --------------------
        float currentFrame = static_cast<float>(glfwGetTime());
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;

        processInput(window);

        // render
        // ------
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // Clear the color and depth buffers

        ourShader.use();

        // view/projection transformations
        glm::mat4 projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100.0f);
        glm::mat4 view = camera.GetViewMatrix();
        ourShader.setMat4("view", view);
        ourShader.setMat4("projection", projection);

        glm::mat4 trans = glm::mat4(1.0f);
        
        float fi = acos(glm::dot(glm::vec3(0.0f, -1.0f, 0.0f), glm::normalize( splineRotations[j])));
        //std::cout << fi << std::endl;
        trans = glm::translate(trans, splinePoints[j]);
        glm::vec3 os = glm::normalize(glm::vec3(-splineRotations[j].z, 0.0f, splineRotations[j].x));
        trans = glm::rotate(trans, fi, os);
        trans = glm::scale(trans, glm::vec3(2.0f, 2.0f, 2.0f));

        ourShader.setMat4("model", trans);
        ourModel.Draw(ourShader);

        shaderProgram.use();
        shaderProgram.setMat4("view", view);
        shaderProgram.setMat4("projection", projection);

        glBindVertexArray(VAO);
        glBindVertexArray(VAO);
        glDrawArrays(GL_LINE_STRIP, 0, splineCombined.size());

        glfwSwapBuffers(window);
        glfwPollEvents();
        ++i;
        if (i % 5 ==0){
            ++j;
        };
    }

    // glfw: terminate, clearing all previously allocated GLFW resources.
    // ------------------------------------------------------------------
    glfwTerminate();
    return 0;
}

// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void processInput(GLFWwindow *window)
{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);

    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
        camera.ProcessKeyboard(FORWARD, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
        camera.ProcessKeyboard(BACKWARD, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
        camera.ProcessKeyboard(LEFT, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
        camera.ProcessKeyboard(RIGHT, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS)
        j = 0;
}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow *window, int width, int height)
{
    // make sure the viewport matches the new window dimensions; note that width and
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);
}

// glfw: whenever the mouse moves, this callback is called
// -------------------------------------------------------
void mouse_callback(GLFWwindow *window, double xposIn, double yposIn)
{
    float xpos = static_cast<float>(xposIn);
    float ypos = static_cast<float>(yposIn);

    if (firstMouse)
    {
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
    }

    float xoffset = xpos - lastX;
    float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top

    lastX = xpos;
    lastY = ypos;

    camera.ProcessMouseMovement(xoffset, yoffset);
}

// glfw: whenever the mouse scroll wheel scrolls, this callback is called
// ----------------------------------------------------------------------
void scroll_callback(GLFWwindow *window, double xoffset, double yoffset)
{
    camera.ProcessMouseScroll(static_cast<float>(yoffset));
}
