import pyglet
import random
import math
window = pyglet.window.Window(width=1600, height=800)
snowflake_image = pyglet.image.load("lab2/snow.bmp")
batch = pyglet.graphics.Batch()

# Create 3 groups for the snowflakes, from back to front
groups = [pyglet.graphics.Group(i) for i in range(3)]
group_speed_list = [400, 500, 1000]
gruup_opacity_list = [True, True, False]
frame = 0
snowflake_count = 30000
ball_sprites = []

class Snowflake(pyglet.sprite.Sprite):
    def __init__(self, img, velocity_x=1, velocity_y=1, scale = 0.1, fade = False,  *args, **kwargs):
        super().__init__(img, *args, **kwargs)
        self.x = random.randint(0, window.width)
        self.y = random.randint(window.height, window.height + 20)
        self.velocity_x = velocity_x
        self.velocity_y = velocity_y
        self.scale = scale
        self.fade = fade
        self.time = 0
        self.random = random.random()*10
    
    def update(self, dt):
        self.time += dt
        self.x += self.velocity_x * math.sin(self.time+ self.random) *dt # Update the x position based on a sine function
        self.y -= self.velocity_y * dt
        if self.fade:
            self.opacity = max(30, self.y / window.height * 255)
        if self.y < -50 or self.x < -50 or self.x > window.width + 50:
            self.visible = False
            self.delete()
            ball_sprites.remove(self)

def create_Snowflake(group: int):
    fade = gruup_opacity_list[group]
    velocity_x = group_speed_list[group]/2
    velocity_y = group_speed_list[group]
    if group == 0:
        scale = random.uniform(0.02, 0.05)
    elif group == 1:
        scale = random.uniform(0.05, 0.1)
    elif group == 2:
        scale = random.uniform(0.05, 0.1)
    snowflake = Snowflake(snowflake_image, velocity_x, velocity_y, scale, fade, batch=batch, group=groups[group])
    
    return snowflake

ball_sprites = []

@window.event
def on_mouse_press(x, y, button, modifiers):
    if button == pyglet.window.mouse.LEFT:
        #random_group = random.choices([0, 1, 2], [group_speed_list[0]/sum(group_speed_list), group_speed_list[1]/sum(group_speed_list), group_speed_list[2]/sum(group_speed_list)])[0]
        snowflake = create_Snowflake(2)
        snowflake.x = x
        snowflake.y = y
        ball_sprites.append(snowflake)
        print("Snowflake created at: ", x, y)

@window.event
def on_draw():
    window.clear()
    batch.draw()

frame = 0

def update(dt):
    if len(ball_sprites) < snowflake_count:
        for i in range(random.randint(0, 2)):
            random_group = random.randint(0,2)
            snowflake = create_Snowflake(random_group)
            ball_sprites.append(snowflake)

    for sprite in ball_sprites:
        sprite.update(dt)

            
    global frame
    frame += 1
    
pyglet.clock.schedule_interval(update, 1/120)  # Call update function 60 times per second
pyglet.app.run()
