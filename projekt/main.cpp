#include <iostream>
#include <cmath>
#include <GL/glut.h> 
#include <vector>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

class RobotArm
{
private:
    int numJoints = 3;
    double *jointAngles;
    double *linkLengths;
    glm::mat4x4 H01;
    glm::mat4x4 H12;
    glm::mat4x4 H23;
    float min_length = 0.0f;
    std::vector<std::vector<std::vector<int>>> binaryMap;
    std::vector<glm::vec3> boundaryPoints;

public:
    RobotArm(float linkLengths[])
    {
        jointAngles = new double[numJoints];
        for (int i = 0; i < numJoints; i++)
        {
            jointAngles[i] = 0.0;
        }

        this->linkLengths = new double[numJoints];
        for (int i = 0; i < numJoints; i++)
        {
            this->linkLengths[i] = linkLengths[i];
        }
        min_length = linkLengths[0] + linkLengths[1] + linkLengths[2];
        H01 = glm::mat4x4(cos(jointAngles[0]), 0.0f, sin(jointAngles[0]), 0.0f,
                          sin(jointAngles[0]), 0.0f, -cos(jointAngles[0]), 0.0f,
                          0.0f, 0.0f, 1.0f, linkLengths[0],
                          0.0f, 0.0f, 0.0f, 1.0f);

        H12 = glm::mat4x4(cos(jointAngles[1]), -sin(jointAngles[1]), 0.0f, linkLengths[1] * cos(jointAngles[1]),
                          sin(jointAngles[1]), cos(jointAngles[1]), 0.0f, linkLengths[1] * sin(jointAngles[1]),
                          0.0f, 0.0f, 1.0f, 0.0f,
                          0.0f, 0.0f, 0.0f, 1.0f);

        H23 = glm::mat4x4(cos(jointAngles[2]), -sin(jointAngles[2]), 0.0f, linkLengths[2] * cos(jointAngles[2]),
                          sin(jointAngles[2]), cos(jointAngles[2]), 0.0f, linkLengths[2] * sin(jointAngles[2]),
                          0.0f, 0.0f, 1.0f, 0.0f,
                          0.0f, 0.0f, 0.0f, 1.0f);

        setJointAngle(1, 0.5 * glm::pi<double>());
        binaryMap = std::vector<std::vector<std::vector<int>>>(50, std::vector<std::vector<int>>(50, std::vector<int>(50, 0)));
    }

    ~RobotArm()
    {
        delete[] jointAngles;
        delete[] linkLengths;
    }
    std::vector<std::vector<std::vector<int>>> getBinaryMap()
    {
        return binaryMap;
    }

    std::vector<glm::vec3> getBoundaryPoints()
    {
        return boundaryPoints;
    }

    float getMinLength()
    {
        return min_length;
    }

    double getJointAngle(int jointIndex)
    {
        if (jointIndex >= 0 && jointIndex < numJoints)
        {
            return jointAngles[jointIndex];
        }
        return 0.0;
    }

    std::vector<double> getLinkLengths()
    {
        std::vector<double> linkLengths;
        for (int i = 0; i < numJoints; i++)
        {
            linkLengths.push_back(this->linkLengths[i]);
        }
        return linkLengths;
    }

    std::vector<double> getJointPoints()
    {
        std::vector<double> jointPoints;

        glm::mat4x4 H02 = H12 * H01;
        glm::mat4x4 H03 = H23 * H02;

        jointPoints.push_back(0.0f);
        jointPoints.push_back(0.0f);
        jointPoints.push_back(0.0f);

        jointPoints.push_back(H01[0][3]);
        jointPoints.push_back(H01[1][3]);
        jointPoints.push_back(H01[2][3]);

        jointPoints.push_back(H02[0][3]);
        jointPoints.push_back(H02[1][3]);
        jointPoints.push_back(H02[2][3]);

        jointPoints.push_back(H03[0][3]);
        jointPoints.push_back(H03[1][3]);
        jointPoints.push_back(H03[2][3]);

        return jointPoints;
    }

    void setJointAngle(int jointIndex, double angle)
    {
        if (jointIndex >= 0 && jointIndex < numJoints)
        {
            jointAngles[jointIndex] = angle;
        }

        H01 = glm::mat4x4(cos(jointAngles[0]), 0.0f, -sin(jointAngles[0]), 0.0f,
                          0.0f, 1.0f, 0.0f, linkLengths[0],
                          sin(jointAngles[0]), 0.0f, cos(jointAngles[0]), 0.0f,
                          0.0f, 0.0f, 0.0f, 1.0f);

        H12 = glm::mat4x4(cos(jointAngles[1]), -sin(jointAngles[1]), 0.0f, linkLengths[1] * cos(jointAngles[1]),
                          sin(jointAngles[1]), cos(jointAngles[1]), 0.0f, linkLengths[1] * sin(jointAngles[1]),
                          0.0f, 0.0f, 1.0f, 0.0f,
                          0.0f, 0.0f, 0.0f, 1.0f);

        H23 = glm::mat4x4(cos(jointAngles[2]), -sin(jointAngles[2]), 0.0f, linkLengths[2] * cos(jointAngles[2]),
                          sin(jointAngles[2]), cos(jointAngles[2]), 0.0f, linkLengths[2] * sin(jointAngles[2]),
                          0.0f, 0.0f, 1.0f, 0.0f,
                          0.0f, 0.0f, 0.0f, 1.0f);
    }
    void setLinkLength(int linkIndex, double length)
    {
        if (linkIndex >= 0 && linkIndex < numJoints)
        {
            linkLengths[linkIndex] = length;
        }
    }
    glm::vec3 forwardKinematics()
    {
        glm::vec4 origin = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);

        glm::mat4x4 H02 = H12 * H01;

        if (H02[1][3] < 0.0f)
        {
            return glm::vec3{glm::vec3(0.0f, 0.0f, -1.0f)};
        }
        glm::mat4x4 endEffector = H23 * H02;

        if (endEffector[1][3] < 0.0f)
        {
            return glm::vec3{glm::vec3(0.0f, 0.0f, -1.0f)};
        }

        return glm::vec3{glm::vec3(endEffector[0][3], endEffector[1][3], endEffector[2][3])};
    }

    std::vector<glm::vec3> robotWorkspace()
    {
        std::vector<glm::vec3> workspace;
        std::vector<glm::vec3> workspace2;
        for (float i = 10; i < 370; i += 3.5)
        {
            std::cout << "Computing robot workspace: " << (int)(i / (360) * 100 - 2) << "%" << std::endl;
            for (float j = 100 + 180; j < 450 + 180; j += 3.5)
            {
                for (float k = 10; k < 350; k += 3.5)
                {
                    setJointAngle(0, i * M_PI / 180);
                    setJointAngle(1, j * M_PI / 180);
                    setJointAngle(2, k * M_PI / 180);

                    float x = forwardKinematics().x;
                    float y = forwardKinematics().y;
                    float z = forwardKinematics().z;

                   if (z != -1.0f)
                    {
                        if (i == 10)
                        {
                            workspace2.push_back(forwardKinematics());
                        }

                        int new_x = (int)((x + 2.0f) * 50.0f / 4.0f);
                        int new_y = (int)((y) * 50.0f / 4.0f);
                        int new_z = (int)((z + 2.0f) * 50.0f / 4.0f);
                        binaryMap[new_x][new_y][new_z] = 1;
                        workspace.push_back(forwardKinematics());
                    }
                }
            }
        }

        for (size_t i = 0; i < binaryMap.size(); i++)
        {
            for (size_t j = 0; j < binaryMap[i].size(); j++)
            {
                for (size_t k = 0; k < binaryMap[i][j].size(); k++)
                {
                    if (binaryMap[i][j][k] == 1)
                    {
                        if (i == 0 || i == binaryMap.size() - 1 || j == 0 || j == binaryMap[i].size() - 1 || k == 0 || k == binaryMap[i][j].size() - 1)
                        {
                            continue;
                        }
                        else if (binaryMap[i - 1][j][k] == 0 || binaryMap[i + 1][j][k] == 0 || binaryMap[i][j - 1][k] == 0 || binaryMap[i][j + 1][k] == 0 || binaryMap[i][j][k - 1] == 0 || binaryMap[i][j][k + 1] == 0)
                        {
                            glm::vec3 point;
                            point.x = (float)i / 50.0f * 4.0f - 2.0f;
                            point.y = (float)j / 50.0f * 4.0f;
                            point.z = (float)k / 50.0f * 4.0f - 2.0f;
                            boundaryPoints.push_back(point);
                        }
                    }
                }
            }
        }

        setJointAngle(0, 370.0f * M_PI / 180);
        setJointAngle(1, M_PI / 180);
        setJointAngle(2, M_PI / 180);
        std::cout << "boundaryPoints.size(): " << boundaryPoints.size() << std::endl;
        return workspace2;
    };
};

RobotArm robotArm = RobotArm(new float[3]{0.0f, 0.0f, 0.0f});
std::vector<double> jointPoints;
std::vector<glm::vec3> workspace;
// Define camera position
float cameraX = 4.0f;
float cameraY = 3.0f;
float cameraZ = 7.0f;
// Initialize OpenGL
int windowWidth = 1400;  // Example value, replace with actual width
int windowHeight = 1200; // Example value, replace with actual height
std::vector<glm::vec3> pointCloud;
bool drawBoundaryPoints = false;
bool drawPointCloud = false;
bool drawBinaryMap = false;
void renderScene(void)
{

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f); 
    GLUquadricObj *quadric = gluNewQuadric();
    gluQuadricNormals(quadric, GLU_SMOOTH);
    glPushMatrix();
    glColor3f(1.0f, 0.0f, 0.0f); 
    gluCylinder(quadric, 0.1, 0.1, 0.2, 32, 32);
    glPopMatrix();

    glViewport(0, 0, windowWidth, windowHeight);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45.0, (double)windowWidth / (double)windowHeight, 0.1, 100.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glEnable(GL_DEPTH_TEST);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    gluLookAt(cameraX, cameraY, cameraZ, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);

    glBegin(GL_QUADS);
    glColor3f(0.05f, 0.05f, 0.05f); // Set the color to grey
    glVertex3d(-2.0, 0.0, -2.0);
    glVertex3d(2.0, 0.0, -2.0);
    glVertex3d(2.0, 0.0, 2.0);
    glVertex3d(-2.0, 0.0, 2.0);
    glEnd();

    glLineWidth(1.0f);
    glBegin(GL_LINES);

    // Draw x-axis
    glColor3f(1.0f, 0.0f, 0.0f); // Red color
    glVertex3d(-3.0, 0.0f, 0.0f);
    glVertex3d(-2.5, 0.0f, 0.0f);
    // Draw y-axis
    glColor3f(0.0f, 1.0f, 0.0f); // Green color
    glVertex3d(-3.0, 0.0, 0.0);
    glVertex3d(-3.0, 0.5, 0.0);
    // Draw z-axis
    glColor3f(0.0f, 0.0f, 1.0f); // Blue color
    glVertex3d(-3.0, 0.0, 0.0);
    glVertex3d(-3.0, 0.0, 0.5);
    glEnd();

    // Draw the base cylinder
    glPushMatrix();
    glTranslatef(0.0f, 0.03f, 0.0f);
    glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
    glColor3f(1.0f, 0.0f, 0.0f); 
    gluCylinder(quadric, 0.05, 0.05, 0.05, 32, 32);
    glPopMatrix();

    for (size_t i = 0; i < jointPoints.size() - 3; i += 3)
    {
        glLineWidth(5.0f);
        glBegin(GL_LINES);

        glColor3f(0.7f, 0.7f, 0.0f);
        glVertex3d(jointPoints[i], jointPoints[i + 1], jointPoints[i + 2]);
        glVertex3d(jointPoints[i + 3], jointPoints[i + 4], jointPoints[i + 5]);
        glEnd();

        glPushMatrix();
        glTranslatef(jointPoints[i], jointPoints[i + 1], jointPoints[i + 2]);
        glColor3f(1.0f, 0.0f, 0.0f);   
        glutSolidSphere(0.03, 20, 20);
        glPopMatrix();
    }
    if (drawBoundaryPoints)
    {

        for (size_t i = 0; i < robotArm.getBoundaryPoints().size(); i++)
        {
            glPushMatrix();
            glTranslatef(robotArm.getBoundaryPoints()[i].x, robotArm.getBoundaryPoints()[i].y, robotArm.getBoundaryPoints()[i].z);
            
            glColor3f(robotArm.getBoundaryPoints()[i].x / 4.0f + 0.5f, robotArm.getBoundaryPoints()[i].y / 4.0f + 0.5f, robotArm.getBoundaryPoints()[i].z / 4.0f + 0.5f); // Red color
            glutSolidSphere(0.005, 20, 20);                                                                                                                               // Adjust the radius and resolution as needed
            glPopMatrix();
        }
    }
    if (drawPointCloud)
    {
        for (size_t i = 0; i < pointCloud.size(); i++)
        {
            glPushMatrix();
            glTranslatef(pointCloud[i].x, pointCloud[i].y, pointCloud[i].z);
            glColor3f(pointCloud[i].x / 4.0f + 0.5f, pointCloud[i].y / 4.0f + 0.5f, pointCloud[i].z / 4.0f + 0.5f); 
            glutSolidSphere(0.005, 20, 20); // Adjust the radius and resolution as needed
            glPopMatrix();
        }
    }

    glutSwapBuffers();
}

void animate(int value)
{
    robotArm.setJointAngle(2, robotArm.getJointAngle(2) + 0);
    jointPoints = robotArm.getJointPoints();

    glm::vec3 endEffector = robotArm.forwardKinematics();
    {
        glPushMatrix();
        glTranslatef(endEffector.x, endEffector.y, endEffector.z);
        glColor3f(1.0f, 0.0f, 0.0f);  
        glutSolidSphere(0.03, 20, 20); 
        glPopMatrix();
    }
    glutPostRedisplay();
    glutTimerFunc(100, animate, 0);
}

void keyboard(unsigned char key, int x, int y)
{
    switch (key)
    {
    case 'a':
        robotArm.setJointAngle(0, robotArm.getJointAngle(0) - 0.01);
        break;
    case 'q':
        robotArm.setJointAngle(0, robotArm.getJointAngle(0) + 0.01);
        break;
    case 's':
        robotArm.setJointAngle(1, robotArm.getJointAngle(1) - 0.01);
        break;
    case 'w':
        robotArm.setJointAngle(1, robotArm.getJointAngle(1) + 0.01);
        break;
    case 'd':
        robotArm.setJointAngle(2, robotArm.getJointAngle(2) - 0.01);
        break;
    case 'e':
        robotArm.setJointAngle(2, robotArm.getJointAngle(2) + 0.01);
        break;
    case '1':
        drawBoundaryPoints = !drawBoundaryPoints;
        break;
    case '2':
        drawPointCloud = !drawPointCloud;
        break;
    }
    glutPostRedisplay();
}

int main(int argc, char **argv)
{
    if (argc != 4)
    {
        std::cerr << "Usage: " << argv[0] << " <link1_length> <link2_length> <link3_length>" << std::endl;
        return 1;
    }

    float linkLengths[3];
    for (int i = 0; i < 3; ++i)
    {
        linkLengths[i] = std::stof(argv[i + 1]);
    }
    robotArm.setLinkLength(0, linkLengths[0]);
    robotArm.setLinkLength(1, linkLengths[1]);
    robotArm.setLinkLength(2, linkLengths[2]);

    jointPoints = robotArm.getJointPoints();
    pointCloud = robotArm.robotWorkspace();

    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
    glutInitWindowSize(1400, 1200);
    glutCreateWindow("RA - Estimacija radnog prostora RRR robotske ruke");
    glutTimerFunc(1000, animate, 0);
    glutDisplayFunc(renderScene);
    glutKeyboardFunc(keyboard);
    glutMainLoop();
    return 0;
}
